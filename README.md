# Mini-projetML_group15

This is the associated repository to Machine Learning development miniproject - UE A, IMT Atlantique 2023.

Contributors:
- DUONG Hoang
- LUC Tan Tho
- MIGUET Yann
- OUHAMMOU Brahim

The brief description for each file is shown in the table below:

<table>
<tr><td>

| File name | Description |
|-----------|-------------|
|bouquet_of_functions.py|functions for data preprocessing, data preparation, training, result visualization, etc|
|main.ipynb|implementation notebook|
|test.py|unit tests of functions|

</td></tr> </table>

## 0. Requirements
Run this command to check/install all necessary dependencies.
```
pip install -r requirements.txt
```

## 1. Content description
### 1.1. Dataset: 
- Chronic Kidney Disease;
- Banknote Authentication Dataset.

### 1.2. Data preprocessing
- Handle no data values by nan;
- Handle data type;
- Replace missing data by proper value;
- Encode categorical data;
- Center and normalize numeric data.
- Apply PCA if requested.

### 1.3. Data preparation
- Train-test split and create cross-validation as well as confusion matrix.
- Print the computation time.

### 1.4. Model of classification
- Support Vector Machine;
- Random Forest;
- Adaboost with Decision Tree estimator;
- Quadratic Discriminant Analysis;
- Multi-layer Perceptron;
- KMeans (unsupervised, cluster vision purpose).

## 2. Result and discussion
(cf. part C of the main notebook)

## 3. Unit test
Some unit tests have been created. To run these tests, you may follow the commands below.

There are 2 classes defined in **test.py**, corresponding to three stages: Data preprocessing, Data preparation, Training.

<table>
<tr><td>

| Class name | Description |
|-----------|-------------|
|TestPreprocessing|verify shape of arrays and nan-value existence after data preprocessing|
|TestPreparation|verify data splitting ratio and class ratio in Stratified K-fold|

</td></tr> </table>

**Class** *TestPreprocessing*

<table>
<tr><td>

| Test name | Description |
|-----------|-------------|
|test_DataSize|verify shape of arrays after data preprocessing|
|test_MissingDataHandling|verify the processing of missing data|
|DataTypeHandling|verify the processing of wrong data type|

</td></tr> </table>

**Class** *TestPreparation*

<table>
<tr><td>

| Test name | Description |
|-----------|-------------|
|test_DataPreparation|verify data splitting ratio and class ratio in Stratified K-fold|

</td></tr> </table>

To run all the tests:
```
python -m unittest test
``` 
If you want to avoid printing so many things in prompt, simply add -b at the end of the command. For example:
```
python -m unittest test -b
``` 
To run a specific test, use this syntax: **python -m unittest module.Class.test_name** as defined in the tables above. For example:
```
python -m unittest test.TestPreprocessing.test_MissingValHandling
``` 
You can also run this command for help. 
```
python -m unittest -h
```
For more information, please visit [this tutorial](https://docs.python.org/3/library/unittest.html).