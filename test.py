import sys  
import unittest
from preprocessing import *
from data_preparation import *
from train import *

class TestPreprocessing(unittest.TestCase):
    
    def test_DataSize(self):
        data, labels = preprocessing('kidney')
        data1, labels1 = preprocessing("banknote")
        self.assertEqual(data.shape[0], labels.shape[0])
        self.assertEqual(data1.shape[0], labels1.shape[0])
    
    def test_MissingValHandling(self):
        data, labels = preprocessing("kidney")
        data1, labels1 = preprocessing("banknote")
        
        self.assertEqual(len(np.where(pd.isna(data))[0]), 0)
        self.assertEqual(len(np.where(pd.isna(data1))[0]), 0)
        self.assertEqual(len(np.where(pd.isna(labels))[0]), 0)
        self.assertEqual(len(np.where(pd.isna(labels1))[0]), 0)
        
    def test_DataTypeHandling(self):
        data, labels = preprocessing("kidney")
        data1, labels1 = preprocessing("banknote")
        
        data_type = [type(x) for x in data]
        data1_type = [type(x) for x in data1]
        labels_type = [type(x) for x in labels]
        labels1_type = [type(x) for x in labels1]
        
        self.assertFalse('object' in data_type)
        self.assertFalse('object' in data1_type)
        self.assertFalse('str' in labels_type)
        self.assertFalse('str' in labels1_type)
    
    
class TestPreparation(unittest.TestCase):
    
    def test_DataPreparation(self):
        data, labels = preprocessing("kidney")
        test_size=0.2
        threshold = 0.02
        X_train, X_test, y_train, y_test, cv = prepare_data(data, labels, test_size=test_size)
        
        # Check data splitting ratio
        self.assertTrue(abs(X_test.shape[0]/data.shape[0] - test_size) <= threshold)
        self.assertTrue(abs(y_test.shape[0]/labels.shape[0] - test_size) <= threshold)
        
        # Check class ratio in cv
        class_ratio = len(y_train[y_train==0])/len(y_train)
        print(len(y_train))

        for train_index, test_index in cv.split(X_train, y_train):
            y_train_cv, y_test_cv = y_train[train_index], y_train[test_index]
            class_ratio_train_cv = len(y_train_cv[y_train_cv==0])/len(y_train_cv)
            class_ratio_test_cv = len(y_test_cv[y_test_cv==0])/len(y_test_cv)
            
            self.assertTrue(abs(class_ratio - class_ratio_train_cv) <= threshold)
            self.assertTrue(abs(class_ratio - class_ratio_test_cv) <= threshold)
            

if __name__ == '__main__':
    unittest.main()