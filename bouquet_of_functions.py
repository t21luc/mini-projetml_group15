# Delete warning messages
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import time

import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.svm import SVC 
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.model_selection import GridSearchCV
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report, confusion_matrix, ConfusionMatrixDisplay
from sklearn.cluster import KMeans

from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold

import numpy as np 
from scipy.stats import mode
from sklearn.metrics import accuracy_score, confusion_matrix

from typing import Literal, get_args

_DATASETS = Literal["kidney", "banknote"]

def preprocessing(dataset: _DATASETS = "kidney", PCA_dims = 0):
    '''
    This function will process data before training

    Parameters
    -----------------
    dataset: str
        Name of dataset: "banknote", "kidney"
    PCA_dims: int
        The decreased feature dimension using the PCA algorithm: if <=0 or > number of features, no PCA is performed

    Returns
    -----------------
    data: np array
        Feature set
    labels: np array
        Ground truth

    if PCA_dims between 1 and number features, prints a 2D (if PCA_dims=2) or a 3D (if PCA_dims>=3) scatter plot of the data
    '''

    # Raise error if the input model is not in available models
    options = get_args(_DATASETS)
    assert dataset in options, f"'{dataset}' is not in {options}"
    assert isinstance(PCA_dims, int) == True, "Argument PCA_dims must be an integer"

    if dataset == "kidney":
        data = pd.read_csv("dataset/kidney_disease.csv")
    else:
        data = pd.read_csv("dataset/data_banknote_authentication.txt", names=['variance', 'skewness', 'curtosis', 'entropy', 'classification'])

    nb_rows = data.shape[0]
    print(f"Total samples: {nb_rows}")

    # Drop id column (if any)
    try:
        data.drop(columns=['id'], inplace=True)
    except:
        pass

    # Handle no data values by nan
    data.replace(r'^\s*\?$', np.nan, regex=True, inplace=True)

    # Fix wrong type columns
    for col in data.columns:
        if data[col].dtypes == 'object':
            try: 
                data[col] = pd.to_numeric(data[col])
            except:
                # Replace spaces in categorical data
                data[col] = data[col].str.strip()

    # Seperate data features and their labels
    labels = data['classification']
    data.drop(columns=['classification'], inplace=True)

    # Convert str classes to bool classes
    if isinstance(labels[0], str):
        mapping = {'ckd': 1, 'notckd': 0}
        labels = labels.map(mapping)

    print(f"Number of Positive labels: {labels.value_counts()[1]}")
    print(f"Number of Negative labels: {labels.value_counts()[0]}")

    # Get index of num/catergorical columns
    categorical_cols = data.columns[data.dtypes==object]
    data[categorical_cols] = data[categorical_cols].astype("category")
    numeric_cols = data.select_dtypes(include=np.number).columns.tolist()

    # Replace missing numeric data by median
    print(f"Number of missing numeric values: { data[numeric_cols].isnull().sum().sum()} ({len(numeric_cols)} columns)")
    
    def print_missing_value_by_col(cols):
        print("----------------------------------------")
        for col in cols:
            print(f"Missing values in {col}: {data[col].isnull().sum()}/{nb_rows}")
        print("----------------------------------------")
    
    print_missing_value_by_col(numeric_cols)

    data[numeric_cols] = data[numeric_cols].fillna(data[numeric_cols].median())

    # Center and normalize numeric data
    def center_and_normalize(data):
        data = data - data.mean()
        max = data.max()
        min = data.min()
        temp_df = pd.DataFrame({'max':max, 'abs_min': min.abs()})
        norm_factor =  temp_df.max(axis=1)
        data = data/norm_factor
        return data
    
    data[numeric_cols] = center_and_normalize(data[numeric_cols])

    # Replace missing categorical data by the most frequent value
    if not categorical_cols.empty:
        print(f"Number of missing categorical values: { data[categorical_cols].isnull().sum().sum()} ({len(categorical_cols)} columns)")
        print_missing_value_by_col(categorical_cols)
        data[categorical_cols] = data[categorical_cols].fillna(data[categorical_cols].mode().iloc[0])
    
        # Encode categorical data
        for col in categorical_cols:
            one_hot = pd.get_dummies(data[col], prefix=col)
            data = data.drop(col, axis=1)
            data = data.join(one_hot)

    labels = pd.Series.to_numpy(labels)
    data = pd.DataFrame.to_numpy(data)

    # PCA PART
    n_inputs, nb_features = data.shape
    if PCA_dims <= 0 or PCA_dims > nb_features:
        return data, labels
    
    data_cov = (1 / (n_inputs-1)) * (data.T @ data)
    
    # perform a deep copy of data_cov in case the data type is Object, uncastable to a more precise type such as float64
    l, c = data_cov.shape
    data_cov_deep_copy = np.zeros((l, c))
    for i in range(l):
        for j in range(c):
            data_cov_deep_copy[i, j] = data_cov[i, j]

    eigval, eigvec = np.linalg.eig(data_cov_deep_copy)
    eigval_abs, eigvec_abs = abs(eigval), abs(eigvec) # in case of complex eigen values
    U = eigvec_abs[:, :PCA_dims]
    new_data = (U.T @ data.T).T
    
    colors = np.array(["blue", "red"])
    title = "representation of the dataset"
    if PCA_dims == 2:
        plt.scatter(new_data[:, 0], new_data[:, 1], color = colors[labels])
        plt.title("A 2D " + title)
        plt.show()
    elif PCA_dims >= 3:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(new_data[:, 0], new_data[:, 1], new_data[:, 2], c = colors[labels])
        plt.title("A 3D " + title)
        plt.show()
    
    

    return new_data, labels

def prepare_data(X, y, test_size=0.2, n_splits=10):
    '''
    This function will split data into train/test set
    and perform cross-validation (selected method: Stratified k-fold) on training set

    Parameters
    -----------------
    X: np array
        feature set
    y: np array
        labels
    test_size: float or int
        float: percentage of test set in the dataset, int: number of samples in test set
    n_split: int > 2
        The number of folds
    
    Returns
    -----------------
    X_train, y_train, X_test, y_test
    
    cv: Stratified k-fold
    '''

    # Split training and test set w.r.t the proportion of test_size
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=1234)
    # Perform Strtified KFold with n_splits times
    cv = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=1234)

    return X_train, X_test, y_train, y_test, cv

_MODELS = Literal["svm", "forest", "adaboost", "qda", "mlp", "kmeans"]

def train(X_train, y_train, cv, estimator: _MODELS = "svm"):
    '''
    This function will train the selected model to fit training set

    Parameters
    -----------------
    X_train: np array
        Training features
    y_train: np array
        Training labels
    cv: Stratified k-fold
    estimator: str
        Available models: 
            svm - Support Vector Machine,
            forest - Random Forest,
            adaboost - Adaboost with Decision Tree estimator,
            qda - Quadratic Discriminant Analysis,
            mlp - Multi-layer Perceptron                      
    
    Returns
    -----------------
    model: trained model (best model in grid search)
    param_grid: hyperparam range, can be used for visualization
    '''

    # Raise error if the input model is not in available models
    options = get_args(_MODELS)
    assert estimator in options, f"'{estimator}' is not in {options}"

    # defining parameter range for each model
    param_grids = {'svm':  
                        {'C': [0.1, 1, 10],  
                        'gamma': [1, 0.1, 0.01], 
                        'kernel': ['poly', 'rbf']},

                  'forest':
                        {'n_estimators': [10,50,100],
                        'criterion': ["gini", "entropy", "log_loss"],
                        'max_depth': [5, 10, 20, 50, 100],
                        'max_features': ["sqrt", "log2"]},

                  'adaboost': 
                        {'n_estimators': [10,50,100],
                        'learning_rate': [0.1, 1.0, 10]},
                                
                  'qda': 
                        {'reg_param': [0.0, 0.1, 0.5, 1.0],
                        'tol': [1e-5, 1e-4]},
                        
                   'mlp':
                        {'hidden_layer_sizes': [(50,25), (100,50), (200,100), (300,150)],
                        'activation': ['logistic', 'tanh'],
                        'beta_1': [0.9, 0.95, 0.99],
                        'beta_2': [0.9, 0.99, 0.999],
                        'early_stopping': [True]},
                    "kmeans" : 
                         {
                         'n_clusters': [2],
                         'algorithm': ["lloyd", "elkan"]}
                         }
    
    models = {'svm': 
                {'name': 'SVM', 'model': SVC(random_state=1234)},
            'forest': 
                {'name': 'Random Forest', 'model': RandomForestClassifier(random_state=1234)},
            'adaboost': 
                {'name': 'Adaboost', 'model': AdaBoostClassifier(random_state=1234)},
            'qda': 
                {'name': 'Quadratic Discriminant Analysis', 'model': QuadraticDiscriminantAnalysis()},
            'mlp':
                {'name': 'Multi-layer Perceptron', 'model': MLPClassifier(random_state=1234)},
            'kmeans' : 
                {'name' : 'Kmeans' , 'model' : KMeans(random_state=1234, n_init='auto') }}

    param_grid = param_grids[estimator]
    print(f"Hyperparams {models[estimator]['name']} to tune:")
    print(param_grid)

    process_time_start = time.time()
    model = GridSearchCV(models[estimator]['model'], param_grid, refit=True, cv=cv)
    model.fit(X_train, y_train)
    process_time_end = time.time()
    process_time = process_time_end - process_time_start
    
    # print best parameter after tuning
    print("----------------------------------------") 
    print(f"Best params for {models[estimator]['name']}: ", model.best_params_) 
    print("----------------------------------------") 
    print("Mean cross-validated score: ", model.best_score_)

    print("#---\033[1m\033[91mProcessing Time\033[0m---#")
    print(str(process_time) + " seconds")

    return model, param_grid

def test(X_test, y_test, model):
    model_predictions = model.predict(X_test) 

    # print classification report 
    print("#---\033[1m\033[92mClassification Report\033[0m---#")
    print(classification_report(y_test, model_predictions, target_names=["Negative", "Positive"])) 
    print("#---\033[1m\033[92mConfusion Matrix\033[0m---#")
    cm = confusion_matrix(y_test, model_predictions)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                 )
    disp.plot()
    plt.show()
    #print(confusion_matrix(y_test, model_predictions))
